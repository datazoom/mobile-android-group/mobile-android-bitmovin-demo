package com.datazoom.android.collector.tester.bitmovin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.bitmovin.player.BitmovinPlayer;
import com.bitmovin.player.BitmovinPlayerView;
import com.bitmovin.player.config.PlayerConfiguration;
import com.bitmovin.player.config.media.SourceConfiguration;

import com.datazoom.android.collector.basecollector.connection_manager.DZBeaconConnector;
import com.datazoom.android.collector.basecollector.event_collector.collector.DZEventCollector;
import com.datazoom.android.collector.basecollector.model.DatazoomConfig;
import com.datazoom.android.collector.basecollector.model.dz_events.Event;
import com.datazoom.android.collector.bitmovin.BitmovinPlayerCollector;
import com.datazoom.android.collector.tester.bitmovin.R;
import com.datazoom.android.collector.tester.bitmovin.utils.Util;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "PlayerActivity";
    @BindView(R.id.edt_server_address)
    EditText edtServerAddress;
    @BindView(R.id.edt_configuration_id)
    EditText edtConfigurationId;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.btnPush)
    Button btnPush;

    @BindView(R.id.bitmovinPlayerView)
    BitmovinPlayerView bitmovinPlayerView;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    BitmovinPlayer bitmovinPlayer;
    String configId, configUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {
        bitmovinPlayer = bitmovinPlayerView.getPlayer();
        btnPush.setVisibility(View.INVISIBLE);
    }

    private void startLog() {
        try {
            LogMonitor.startAppendingLogsTo(new File(getFilesDir(), "datazoom-log.log"));
        } catch (IOException e) {
            Log.e(TAG, "Cannot create file:datazoom-log.log");
        }
    }

    @OnClick(R.id.btn_submit)
    public void submit() {
        btnSubmit.setVisibility(View.INVISIBLE);
        btnPush.setVisibility(View.VISIBLE);
        btnSubmit.setEnabled(false);

        hideKeyboard();
        getConfig();
    }

    private void getConfig() {
        configId = edtConfigurationId.getText().toString();
        configUrl = edtServerAddress.getText().toString();

        if (!configId.trim().equals("") && !configUrl.trim().equals("")) {
            connectPlayer();
        }
    }

    private void connectPlayer() {
        progressBar.setVisibility(View.VISIBLE);
        PlayerConfiguration config = PlayerConfiguration.fromJSON(Constants.playerConfigKey);
        bitmovinPlayer.setup(config);
        BitmovinPlayerCollector.create(bitmovinPlayer, MainActivity.this)
                .setConfig(new DatazoomConfig(configId, configUrl))
                .connect(new DZBeaconConnector.ConnectionListener() {
                    @Override
                    public void onSuccess(DZEventCollector dzEventCollector) {
                        startLog();
                        try {
                            dzEventCollector.setDatazoomMetadata(new JSONArray( "[{\"customPlayerName\": \"Bitmovin Player\"},{\"customDomain\": \"devplatform.io\"}]  "));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Event event = new Event("SDKLoaded",new JSONArray());
                        dzEventCollector.addCustomEvent(event);

                        btnPush.setOnClickListener(v -> {

                            try {
                                Event event1 = new Event("btnPush",new JSONArray( "[{\"customPlay\": \"true\"}]  ") );
                                dzEventCollector.addCustomEvent(event1);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        });

                        progressBar.setVisibility(View.GONE);
                        play();
                    }

                    @Override
                    public void onError(Throwable t) {
                        btnSubmit.setEnabled(true);
                        progressBar.setVisibility(View.GONE);
                        Util.showToastShort(MainActivity.this, Constants.connectionFailed);
                    }
                });
    }

    private void play() {
        bitmovinPlayerView.setVisibility(View.VISIBLE);
        SourceConfiguration sourceConfiguration = new SourceConfiguration();
        sourceConfiguration.addSourceItem("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8");
        bitmovinPlayer.load(sourceConfiguration);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getApplicationContext());
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (bitmovinPlayer != null && bitmovinPlayer.isPlaying())
            bitmovinPlayer.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            LogMonitor.stopMonitoring();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

}
